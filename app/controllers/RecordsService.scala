package controllers

import java.io.{ BufferedReader, File }
import java.nio.charset.Charset
import java.nio.file._
import models._
import play.api._
import play.api.libs.json._
import play.api.mvc._
import scala.concurrent.Future
import scala.slick.driver.PostgresDriver.simple._
import scala.util.{ Try, Success, Failure }

object RecordsService extends ApiController with DBConnection {
  implicit val recordWrites = Records.jsonWrites

  def list(offset: Int, limit: Int) = Action { implicit request =>
    defaultDB withSession { implicit session =>
      val (rs, total): (List[Record], Int) = defaultDB withSession { implicit session =>
        val filtered = Records.table
        val paginated = filtered.drop(offset).take(limit)
        (paginated.list, filtered.size.run)
      }

      val navLinks: Map[String, JsValue] = navOffsets(offset, limit, total) map {
        case (name, off) =>
          name -> Json.obj("href" -> routes.RecordsService.list(off, limit).absoluteURL())
      }

      val _links = Json.obj(
        "curies" -> defaultCurie,
        "self" -> Json.obj("href" -> routes.RecordsService.list(offset, limit).absoluteURL()),
        "up" -> Json.obj("href" -> routes.Application.apiIndex.absoluteURL()),
        "type" -> "collection"
      ) ++ JsObject(navLinks.toSeq)

      val obj = Json.obj(
        "_links" -> _links,
        "count" -> rs.size,
        "total" -> total,
        "_embedded" -> Json.obj(
          "metabodb:records" -> Json.toJson(rs)
        )
      )
      Ok(Json.prettyPrint(obj))
    }
  }

  def show(id: String) = TODO

  def upload = Action.async(parse.temporaryFile) { implicit request =>
    import scala.concurrent.ExecutionContext.Implicits.global

    Future {
      def save: Try[Path] = Try {
        val tmpFile = Paths.get(s"./${java.util.UUID.randomUUID.toString}")
        request.body.moveTo(new File(tmpFile.toString))
        Logger.info(s"Saved import spreadsheet to ${tmpFile}")
        tmpFile
      }

      def insert(path: Path): Try[(Path, String)] = Try {
        val charset = Charset.forName("UTF-8")
        val stream = Files.newBufferedReader(path, charset)

        val (readLines, savedRecords): (Int, Int) =
          defaultDB withSession { implicit session =>
            @scala.annotation.tailrec
            def loop(reader: BufferedReader, read: Int, saved: Int): (Int, Int) = {
              reader.readLine match {
                case line: String =>
                  Records.parse(line) match {
                    case Right(rec) =>
                      val res = Records.table += rec
                      loop(reader, read + 1, saved + res)
                    case Left(msg) =>
                      Logger.warn(msg)
                      loop(reader, read + 1, saved)
                  }
                case _ => (read, saved)
              }
            }

            loop(stream, 0, 0)
          }
        (path, s"Successfully saved ${savedRecords} of ${readLines} lines in file")
      }

      save flatMap insert match {
        case Success((path, msg)) =>
          Logger.info(msg)
          Ok(msg)
        case Failure(err) =>
          Logger.error(err.getMessage)
          InternalServerError(err.getMessage)
      }
    }
  }
}
