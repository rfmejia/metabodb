package controllers

import models._
import play.api._
import play.api.libs.json._
import play.api.mvc._
import scala.slick.driver.PostgresDriver.simple._

object Application extends ApiController with DBConnection {

  def index = Action { implicit request =>
    val totalRecords = defaultDB withSession { implicit session =>
      Records.table.size.run
    }

    Ok(views.html.index("v1", totalRecords))
  }

  def apiIndex = Action { implicit request =>
    val totalRecords = defaultDB withSession { implicit session =>
      Records.table.size.run
    }

    val _links = Json.obj(
      "curies" -> defaultCurie,
      "self" -> Json.obj("href" -> routes.Application.apiIndex.absoluteURL()),
      "profile" -> Json.obj("href" -> "metabodb:service"),
      "metabodb:records" -> Json.obj(
        "href" -> routes.RecordsService.list().absoluteURL(),
        "title" -> "List of records",
        "total" -> totalRecords
      )
    )

    val obj = Json.obj(
      "_links" -> _links
    )

    Ok(Json.prettyPrint(obj))
  }

  def getSchema = Action {
    Ok(Records.table.ddl.createStatements.mkString(";\n"))
  }

  def showProfile(profile: String) = Action {
    Option(profile) map {
      case "records" => s"${profile}: List of metabodb:record instances."
      case "record" => s"${profile}: Metabolite record in the format translated from the initial spreadsheet to JSON. `N/A` values are shown as `null`. Columns with multiple possible values (i.e., PubChem CID, database IDs) are converted into JSON arrays."
      case _ => s"${profile}: Not found"
    } match {
      case Some(msg) => Ok(msg)
      case None => NotFound
    }
  }
}
