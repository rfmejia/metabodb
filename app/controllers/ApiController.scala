package controllers

import play.api.libs.json._
import play.api.mvc._

trait ApiController extends Controller {

  def defaultCurie(implicit r: RequestHeader) = Json.obj(
    "metabodb" -> (routes.Application.showProfile("").absoluteURL() + "{rel}"),
    "templated" -> true
  )

  def navOffsets(offset: Int, limit: Int, total: Int): Map[String, Int] = {
    val startLinks = if (offset > 0) {
      Map(
        "first" -> 0,
        "prev" -> Math.max(offset - limit, 0)
      )
    } else Map.empty
    val endLinks = if (offset + limit < total) {
      Map(
        "next" -> Math.min(offset + limit, total),
        "last" -> Math.max(total - limit, 0)
      )
    } else Map.empty
    (startLinks ++ endLinks)
  }
}
