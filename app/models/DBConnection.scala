package models

import play.api.db.DB
import play.api.Play.current
import scala.slick.driver.PostgresDriver.simple._

trait DBConnection {
  val defaultDB = Database.forDataSource(DB.getDataSource())
}
