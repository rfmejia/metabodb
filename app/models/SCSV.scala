package models

import play.api.libs.json._
import scala.slick.driver.PostgresDriver.simple._

/**
 * A list of semicolon-separated ';' values.
 */
case class SCSV(values: List[String]) {
  override def toString = values.mkString(";")
}

object SCSV {
  def parse(s: String): SCSV = {
    val items: List[String] = s.split(";").toList map (_.trim) filter (!_.isEmpty)
    SCSV(items)
  }

  val slickMapper = MappedColumnType.base[SCSV, String](_.toString, SCSV.parse(_))

  val jsonWrites = new Writes[SCSV] {
    def writes(scsv: SCSV) = JsArray(scsv.values map (JsString(_)))
  }
}

