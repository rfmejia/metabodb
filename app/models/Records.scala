package models

import play.api.Logger
import play.api.libs.json._
import scala.slick.driver.PostgresDriver.simple._
import scala.util.{ Try, Success, Failure }

case class Record(
  id: Option[Int],
  title: String,
  inchiKey: String,
  shortInchiKey: String,
  pubChemCID: Option[SCSV],
  exactMass: Double,
  formula: String,
  smiles: String,
  chebi: Option[SCSV],
  hmdb: Option[SCSV],
  knapsack: Option[SCSV],
  plantcyc: Option[SCSV],
  pubchem: Option[SCSV],
  smpdb: Option[SCSV],
  unpd: Option[SCSV],
  ymdb: Option[SCSV]
)

object Records {
  val table = TableQuery[Model]

  def parse(s: String): Either[String, Record] = {
    def stringToOption(str: String) = str match {
      case "" => None
      case "N/A" => None
      case _ => Some(str.replaceAll("\"", ""))
    }

    def lineToRecord(line: List[Option[String]]) = line match {
      case Some(title) :: Some(inchiKey) :: Some(shortInchiKey) :: pubChemCID :: Some(exactMass) :: Some(formula) :: Some(smiles)
        :: more =>
        Try {
          val _pubChemCID = pubChemCID map SCSV.parse
          more map (_ map SCSV.parse) match {
            case chebi :: hmdb :: knapsack :: plantcyc :: pubchem :: smpdb :: unpd :: ymdb :: Nil =>
              Record(None, title, inchiKey, shortInchiKey, pubChemCID map SCSV.parse, exactMass.toDouble, formula, smiles,
                chebi, hmdb, knapsack, plantcyc, pubchem, smpdb, unpd, ymdb)
            case _ => throw new IllegalArgumentException("Line had an incorrect number of columns")
          }
        } match {
          case Success(rec) => Right(rec)
          case Failure(err) =>
            err.printStackTrace
            Logger.error(err.getMessage)
            Left(err.getMessage)
        }
      case _ => Left("Incorrect line format")
    }

    val line: List[Option[String]] = s.split("\t").toList map (stringToOption)
    Logger.debug(line.toString);
    lineToRecord(line)
  }

  class Model(tag: Tag) extends Table[Record](tag, "RECORDS") {
    implicit val mapper = SCSV.slickMapper

    def id = column[Int]("ID", O.PrimaryKey, O.AutoInc)
    def title = column[String]("TITLE", O.NotNull, O.DBType("TEXT"))
    def inchiKey = column[String]("INCHI_KEY", O.NotNull, O.DBType("TEXT"))
    def shortInchiKey = column[String]("SHORT_INCHI_KEY", O.NotNull, O.DBType("TEXT"))
    def pubChemCID = column[Option[SCSV]]("PUBCHEM_CID", O.DBType("TEXT"))
    def exactMass = column[Double]("EXACT_MASS", O.NotNull)
    def formula = column[String]("FORMULA", O.NotNull, O.DBType("TEXT"))
    def smiles = column[String]("SMILES", O.NotNull, O.DBType("TEXT"))

    def chebi = column[Option[SCSV]]("CHEBI", O.DBType("TEXT"))
    def hmdb = column[Option[SCSV]]("HMDB", O.DBType("TEXT"))
    def knapsack = column[Option[SCSV]]("KNAPSACK", O.DBType("TEXT"))
    def plantcyc = column[Option[SCSV]]("PLANTCYC", O.DBType("TEXT"))
    def pubchem = column[Option[SCSV]]("PUBCHEM", O.DBType("TEXT"))
    def smpdb = column[Option[SCSV]]("SMPDB", O.DBType("TEXT"))
    def unpd = column[Option[SCSV]]("UNPD", O.DBType("TEXT"))
    def ymdb = column[Option[SCSV]]("YMDB", O.DBType("TEXT"))

    def * = (id.?, title, inchiKey, shortInchiKey, pubChemCID, exactMass, formula, smiles,
      chebi, hmdb, knapsack, plantcyc, pubchem, smpdb, unpd, ymdb) <> (Record.tupled, Record.unapply)
  }

  val jsonWrites = new Writes[Record] {
    implicit val scsvWrites = SCSV.jsonWrites

    def writes(record: Record) = Json.obj(
      "_links" -> Json.obj(
        "profile" -> Json.obj("href" -> "metabodb:record") 
      ),
      "id" -> record.id,
      "title" -> record.title,
      "inchiKey" -> record.inchiKey,
      "shortInchiKey" -> record.shortInchiKey,
      "exactMass" -> record.exactMass,
      "formula" -> record.formula,
      "smiles" -> record.smiles,
      "pubChemCID" -> record.pubChemCID,

      "chebi" -> record.chebi,
      "hmdb" -> record.hmdb,
      "knapsack" -> record.knapsack,
      "plantcyc" -> record.plantcyc,
      "pubchem" -> record.pubchem,
      "smpdb" -> record.smpdb,
      "unpd" -> record.unpd,
      "ymdb" -> record.ymdb
    )
  }
}
