name := """metabodb"""

version := "1.0"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

scalacOptions ++= Seq(
  "-Xlint",
  "-deprecation", 
  "-Xfatal-warnings",
  "-feature", 
  "-unchecked", 
  "-encoding", 
  "utf8")

resolvers += Resolver.sonatypeRepo("releases")

val databaseDependencies = Seq(
  "com.typesafe.play" %% "play-slick" % "0.8.1",
  "org.postgresql" % "postgresql" % "9.4-1201-jdbc41"
)

val apiDependencies = Seq(
  "com.netaporter" %% "scala-uri" % "0.4.6"
)

libraryDependencies ++= databaseDependencies ++ Seq(
  jdbc,
  anorm,
  cache,
  ws
)

defaultScalariformSettings

herokuJdkVersion in Compile := "1.8"

herokuAppName in Compile := "metabodb"

herokuProcessTypes in Compile := Map(
  "web" -> "target/universal/stage/bin/metabodb -Dhttp.port=${PORT} -DapplyEvolutions.default=true -Ddb.default.driver=org.postgresql.Driver -Ddb.default.url=${DATABASE_URL}" 
)
